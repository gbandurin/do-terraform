output "droplet_ip" {
  value = digitalocean_droplet.vps.*.ipv4_address
}

# Local return true if loadbalancer presented in list
locals {
  find_lb = contains(split("-", element(var.devs, 0)), "lb")
}

resource "local_file" "output_result" {
  content = <<EOF

lb:
  hosts:
    %{if local.find_lb == true}${element(aws_route53_record.my_name.*.fqdn, 0)}%{else} %{endif}

webservers:
  hosts:
    %{if local.find_lb == true}${join("\n", slice(aws_route53_record.my_name.*.fqdn, 1, length(var.devs)))}%{else}${join("\n    ", aws_route53_record.my_name.*.fqdn)}%{endif}

  EOF


  filename = "${path.module}/hosts.yml"
}

resource "null_resource" "yaml_mover" {
  triggers = {
    version = timestamp()
  }

  provisioner "local-exec" {
    command = "cp ${path.module}/hosts.yml ${var.inventory_path}/hosts.yml"
  }
  depends_on = [local_file.output_result]
}
