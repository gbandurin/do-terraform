# Credentials - no default value
variable "do_token" {
  type = string
}

variable "access_aws" {
  type = string
}

variable "secret_aws" {
  type = string
}

# Personal - domain name
variable "dnszone_aws" {
  type = string
}

# Personal - subdomain and droplet names
variable "devs" {
  type = list(any)
}

# Personal - resource tags
variable "tags" {
  type = list(any)
}

# Personal - path to copy hosts.yaml
variable "inventory_path" {
  type = string
}

# Parameters - have default values
variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "drolplet_image" {
  type    = string
  default = "ubuntu-18-04-x64"
}

variable "do_region" {
  type    = string
  default = "fra1"
}

variable "droplet_size" {
  type    = string
  default = "s-1vcpu-1gb"
}

