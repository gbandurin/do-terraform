# Create a new SSH key
resource "digitalocean_ssh_key" "default" {
  name       = "g.bandurin"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "digitalocean_tag" "my_tag" {
  count = length(var.tags)
  name  = element(var.tags, count.index)
}

# Create a new Droplet using the SSH key
resource "digitalocean_droplet" "vps" {
  count    = length(var.devs)
  image    = var.drolplet_image
  name     = element(var.devs, count.index)
  region   = var.do_region
  size     = var.droplet_size
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
  tags     = digitalocean_tag.my_tag.*.id
}

data "aws_route53_zone" "hosted_zone" {
  name = var.dnszone_aws
}

# Creating domain name

resource "aws_route53_record" "my_name" {
  count   = length(var.devs)
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = join(".", [element(var.devs, count.index), data.aws_route53_zone.hosted_zone.name])
  type    = "A"
  ttl     = "300"
  records = [element(digitalocean_droplet.vps.*.ipv4_address, count.index)]

  depends_on = [digitalocean_droplet.vps]
}

